import 'package:covid19/src/pages/Home-menu.dart';
import 'package:covid19/src/pages/informacion.dart';
import 'package:flutter/material.dart';

 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: '2',textTheme: TextTheme(title: TextStyle(fontFamily: '2'))),
      title: 'C-19',
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context)=> Home(),
        'informacion': (BuildContext context)=> Informacion(),
      },
    );
  }
}