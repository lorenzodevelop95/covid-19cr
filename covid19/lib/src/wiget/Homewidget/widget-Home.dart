import 'package:covid19/src/Models/model.dart';
import 'package:covid19/src/wiget/Homewidget/widget-cuadrados.dart';
import 'package:flutter/material.dart';

//List<Datum> data

Widget fondoPrincipal(double w,double _sizeh, List<Datum> data) {
  return Stack(
    children: <Widget>[
      ClipRRect(
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
        child: Container(
          color: Colors.teal.shade600,
          height:(_sizeh > 640) ? _sizeh/2.3:_sizeh/2.4,
          width: double.infinity,
         
        ),
      ),
      fondo(w,_sizeh, data)
    ],
  );
}

Widget fondo(double w,double _sizeh, List<Datum> data) {
  print(_sizeh);
  print(w);
  return Stack(
    
    alignment: AlignmentDirectional.center,
    children: <Widget>[
    Positioned(
      top: 0,
      right:(_sizeh < 540) ?80:(_sizeh > 740) ?null:_sizeh/10 ,
          child: Center(
            child: Container(
            color: Colors.transparent,
            //  height: _sizeh/3.5,
            //  width: w*0.9,
            child: Image(
              
              // (_sizeh > 640) ? 350 : 300
              height: (_sizeh > 682) ?_sizeh/2.7:_sizeh/2.5,
              //  _tamaoofondo(_sizeh),
             
              image: AssetImage('assets/dos.png'),
            )),
          ),
    ),
    Positioned(
      // (_sizeh > 640) ? 50 : 35
      left: w*0.2,
      top: _sizeh/15,
      // (_sizeh > 640) ? 90 : 20
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          color: Colors.transparent,
          height: 50,
          width: 50,
          child: Stack(
            children: <Widget>[
              //Image(image: AssetImage('assets/vi.png')),
              Center(
                  child: Text(
                data[0].byLocation.guanacaste.toString(),
                style: TextStyle(
                  fontFamily: 'letra',
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                  color: Colors.white,
                  // letterSpacing: 2
                ),
              )),
            ],
          ),
        ),
      ),
    ),
    Positioned(
      left: (_sizeh > 640) ? (_sizeh > 682)?_sizeh/3.9:_sizeh/3.9:(_sizeh < 585) ? _sizeh/3.4:_sizeh/3.3,
      // (_sizeh > 640) ? 80 : 20
      top:  (_sizeh > 640) ?_sizeh/14:_sizeh/25,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
                //Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.heredia.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
    Positioned(
      left: (_sizeh > 640) ? (_sizeh > 682)? _sizeh/5:_sizeh/4.8:_sizeh/4,
      top: (_sizeh > 640) ?_sizeh/15:_sizeh/30,
      // (_sizeh > 640) ? 70 : 20,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
                //Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.alajuela.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
    Positioned(
      right: (_sizeh > 640) ?(_sizeh > 740) ? w/4.5:w/4: w/4,
      // (_sizeh > 640) ? 40 : 30,
      top:  (_sizeh > 640) ? _sizeh/7: _sizeh/7,
      // _posiciolimon(_sizeh)
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
               // Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.limon.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
    Positioned(
      //2.7
      left: (_sizeh > 640) ? (_sizeh > 682)? _sizeh/3.5:_sizeh/3.2:(_sizeh < 585) ? _sizeh/3:_sizeh/2.7,
      top:  (_sizeh > 640) ? (_sizeh > 682)? _sizeh/8: _sizeh/7:_sizeh/8.5,
      // (_sizeh > 640) ? 140 : 70,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
               // Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.cartago.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
    Positioned(
      left: (_sizeh > 640) ? (_sizeh > 740)? _sizeh/4.5:_sizeh/4.5:(_sizeh < 640) ? _sizeh/ 3.8:_sizeh/ 3.5,
      top:  (_sizeh > 640) ? _sizeh/7.5: _sizeh/8,
      // (_sizeh > 640) ? 150 : 90
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
                //Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.sanJose.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
    // Positioned(
    //   top: _sizeh/4,
    //    left: w/50, 
    //    child: fecha(data)
      
    //   ),
     
    Positioned(
      right: (_sizeh > 640) ?  w/4:w/3.5,
      // (_sizeh > 640) ? 50 : 40
      //,
      top: (_sizeh > 640) ?  _sizeh/4.5: _sizeh/4.5,
      // (_sizeh > 640) ? 60 : 10
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            height: 50,
            width: 50,
            child: Stack(
              children: <Widget>[
                //Image(image: AssetImage('assets/vi.png')),
                Center(
                    child: Text(
                  data[0].byLocation.puntarenas.toString(),
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )),
              ],
            )),
      ),
    ),
  ]);
}

double _tamaoofondo(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 300;
  } else if (size > 640 && size < 740) {
    tama = 330;
  } else {
    tama = 210;
  }

  return tama;
}

double _posiciolimon(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 160;
  } else if (size > 640 && size < 740) {
    tama = 140;
  } else {
    tama = 80;
  }

  return tama;
}

double _posicioalajuela(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 210;
  } else if (size > 640 && size < 740) {
    tama = 210;
  } else {
    tama = 110;
  }

  return tama;
}

double _posiciohe(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 150;
  } else if (size > 640 && size < 740) {
    tama = 150;
  } else {
    tama = 150;
  }

  return tama;
}
