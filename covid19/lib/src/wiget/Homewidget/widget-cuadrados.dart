import 'package:covid19/src/Models/model.dart';
import 'package:flutter/material.dart';

Widget widgetcuadradoprincipal(
    double w, double _sizeh, String data, String nombre, String ico) {

      String activo='ic_infected.png';
      String descartado='ic_descarted.png';
  // confirmeCase
  return Card(
    elevation: 10,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 7: _sizeh/7,
            // (_sizeh > 640) ? 130 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 130
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    nombre,
                    style: TextStyle(
                      fontFamily: 'letra',
                      
                      fontSize:(_sizeh > 640) ?  13:12,
                      // fontWeight: FontWeight,
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          //color: Colors.red,
                          height: (_sizeh > 640) ? 50:30,
                          width: (_sizeh > 640) ? 50:30,
                          image: (ico == 'activo')?AssetImage('assets/$activo'):AssetImage('assets/$descartado')
                          )
                          ),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(data,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}

//

double posicicuadros(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 20;
  } else if (size > 640 && size < 740) {
    tama = 10;
  } else {
    tama = 30;
  }

  return tama;
}

double posicihorizontal(double size) {
  double tama;
  if (size > 640 && size > 740) {
    tama = 5;
  } else if (size > 640 && size < 740) {
    tama = 10;
  } else {
    tama = 10;
  }

  return tama;
}

Widget prueba(double _sizeh, String data, String nombre) {
  return Card(
    elevation: 10,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.teal, Colors.teal])),
            height: 100,
            width: (_sizeh > 640) ? 120 : 100,
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Center(
                      child: Text(
                    nombre,
                    style: TextStyle(
                        fontFamily: 'letra',
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                        // letterSpacing: 2,
                        color: Colors.white),
                  )),
                  Center(
                      child: Text(
                          // data[0].byStatus.active.toString(),
                          data,
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 40,
                              //fontWeight: FontWeight.w800,
                              // letterSpacing: 2,
                              color: Colors.white))),
                ],
              ),
            ))),
  );
}

Widget fecha(List<Datum> data) {
  return Row(
    children: <Widget>[
      Text(
        'Fecha :',
        style: TextStyle(
            fontFamily: 'letra',
            fontSize: 18,
            fontWeight: FontWeight.w800,
            color: Colors.white,
            letterSpacing: 1),
      ),
      Text(
        ' ${data[0].date.day}/',
        style: TextStyle(
            fontFamily: 'letra',
            fontSize: 18,
            fontWeight: FontWeight.w800,
            color: Colors.white,
            letterSpacing: 1),
      ),
      Text(
        '${data[0].date.month.toString()}/',
        style: TextStyle(
            fontFamily: 'letra',
            fontSize: 18,
            fontWeight: FontWeight.w800,
            color: Colors.white,
            letterSpacing: 1),
      ),
      Text(
        '${data[0].date.year.toString()}',
        style: TextStyle(
            fontFamily: 'letra',
            fontSize: 18,
            fontWeight: FontWeight.w800,
            color: Colors.white,
            letterSpacing: 1),
      )
    ],
  );
}


Widget widgetActivos(double w, double _sizeh, String data, String nombre, String ico) {
  // confirmeCase
  return Card(
  elevation: 10,
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
  child: ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.centerLeft,
                  colors: [Colors.white, Colors.white])),
          height: (_sizeh > 640) ? _sizeh / 9:_sizeh/10,
          // (_sizeh > 640) ? 130 : 130
          width: w * 0.50,
          // (_sizeh > 640) ? 150 : 130
          child: Container(
            padding: EdgeInsets.only(top: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Center(
                    child: Text(
                  nombre,
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: (_sizeh > 640) ? 13:12,
                   // fontWeight: FontWeight.w800,
                    color: Colors.black54,
                    // letterSpacing: 2
                  ),
                )),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image(
                            color: Colors.red,
                            height: (_sizeh > 640) ? 50:30,
                            width:  (_sizeh > 640) ?50:30,
                            image: AssetImage('assets/ic_active_patients.png'))),
                    SizedBox(
                       width: w/9,
                    ),
                    Center(
                        child: Text(data,
                            // data[0].confirmedCases.toString(),
                            style: TextStyle(
                                fontFamily: 'letra',
                                fontSize: 18,
                                //fontWeight: FontWeight.w800,
                                // letterSpacing: 2,
                                color: Colors.black54))),
                  ],
                )
              ],
            ),
          ))),
    );
}

Widget recuperados(double w, double _sizeh, String data, String nombre) {
  // confirmeCase
  return Card(
    elevation: 10,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ? _sizeh / 11:_sizeh / 10,
            // (_sizeh > 640) ? 130 : 130
            width: w/2.5,
            // (_sizeh > 640) ? 150 : 130
            child: Container(
              padding: EdgeInsets.only(top: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    nombre,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize: 12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image(
                             // color: Colors.red,
                              height: (_sizeh > 640) ? 40:30,
                              width: (_sizeh > 640) ? 40:30,
                              image: (nombre == 'Pacientes Recuperados')?AssetImage('assets/ic_patients_restored.png'):AssetImage('assets/ic_patients_dead.png'))),
                      SizedBox(
                         width: (_sizeh > 640) ? w/9:w/15,
                      ),
                      Center(
                          child: Text(data,
                              // data[0].confirmedCases.toString(),
                              style: TextStyle(
                                  fontFamily: 'letra',
                                  fontSize: 18,
                                  //fontWeight: FontWeight.w800,
                                  // letterSpacing: 2,
                                  color: Colors.black54))),
                    ],
                  )
                ],
              ),
            ))),
  );
}

Widget condicionpa(double w, double _sizeh, String data, String nombre) {
  // confirmeCase
  return Card(
    elevation: 0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.teal, Colors.teal])),
            height: _sizeh / 18,
            // (_sizeh > 640) ? 130 : 130
            width: w/1.9,
            // (_sizeh > 640) ? 150 : 130
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                 Text(
                  nombre,
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )
                
              ],
            ))),
  );
}

Widget condicioncaso(double w, double _sizeh, String data, String nombre) {
  // confirmeCase
  return Card(
    elevation: 0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
            color: Colors.teal.withOpacity(0.8),
            height: _sizeh / 18,
            // (_sizeh > 640) ? 130 : 130
            width: w/1.9,
            // (_sizeh > 640) ? 150 : 130
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                 Text(
                  nombre,
                  style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    // letterSpacing: 2
                  ),
                )
                
              ],
            ))),
  );
}


