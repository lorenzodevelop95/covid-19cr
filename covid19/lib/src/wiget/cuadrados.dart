import 'package:covid19/src/Models/model.dart';
import 'package:flutter/material.dart';

//List<Datum> data
Widget cuadrados(
    double w, double _sizeh, String data, String nombre) {
     
        // String hombre='Hombres.png';
        //  String hombre='Hombres.png';
        //   String hombre='Hombres.png';



  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  // ClipRRect(
                  //   borderRadius: BorderRadius.circular(100),
                  //   child: Container(
                  //     height: 60,
                  //     width: 60,

                  //     child: Image.asset('assets/ma.png'),
                  //   ),
                  // ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          color:  Colors.pink.withOpacity(0.7),
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/Mujeres.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                              
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}
Widget cuadradoH(
    double w, double _sizeh, String data, String nombre) {
     
        // String hombre='Hombres.png';
        //  String hombre='Hombres.png';
        //   String hombre='Hombres.png';



  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                      
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  // ClipRRect(
                  //   borderRadius: BorderRadius.circular(100),
                  //   child: Container(
                  //     height: 60,
                  //     width: 60,

                  //     child: Image.asset('assets/ma.png'),
                  //   ),
                  // ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          color:  Colors.blue.withOpacity(0.7),
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/Hombres.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                              
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}
Widget cuadradoN(
    double w, double _sizeh, String data, String nombre) {
     
        // String hombre='Hombres.png';
        //  String hombre='Hombres.png';
        //   String hombre='Hombres.png';



  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                      
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                         
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/nina.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                             
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}
Widget cuadradoJ(
    double w, double _sizeh, String data, String nombre) {
     
        // String hombre='Hombres.png';
        //  String hombre='Hombres.png';
        //   String hombre='Hombres.png';



  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  // ClipRRect(
                  //   borderRadius: BorderRadius.circular(100),
                  //   child: Container(
                  //     height: 60,
                  //     width: 60,

                  //     child: Image.asset('assets/ma.png'),
                  //   ),
                  // ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                         
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/joven.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                              
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}


Widget horizontal(double sizeh, String nombre, data) {
  return Card(
    elevation: 10,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.teal, Colors.teal])),
            height: 100,
            width: (sizeh > 640) ? 120 : 100,
            //color: Colors.red.withOpacity(0.7),
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Center(
                      child: Text(
                    nombre,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize: 20,
                    
                      color: Colors.white,
                      // letterSpacing: 2
                    ),
                  )),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                      child: Text(
                    data,
                    // data[0].byAge.adults.toString(),
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize: 35,
                      
                      color: Colors.white,
                      // letterSpacing: 2
                    ),
                  )),
                ],
              ),
            ))),
  );
}



Widget horizontal2(double sizeh, String nombre, data) {
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.teal, Colors.teal])),
            height: 150,
            width: (sizeh > 640) ? 120 : 100,
            //color: Colors.red.withOpacity(0.7),
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                      child: Text(
                    nombre,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize: (sizeh > 640) ? 16 : 12,
                      
                      color: Colors.white,
                      // letterSpacing: 2
                    ),
                  )),
                  SizedBox(
                    height: 25,
                  ),
                  Center(
                      child: Text(
                    data,
                    // data[0].byNationality.costarricans.toString(),
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:40,
                     
                      color: Colors.white,
                      // letterSpacing: 2
                    ),
                  )),
                ],
              ),
            ))),
  );
}


Widget cuadradoc(
    double w, double _sizeh, String data, String nombre) {
     
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6.5: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.3,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/CR.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                             
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}

Widget cuadradoF(
    double w, double _sizeh, String data, String nombre) {
     
       


  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6.5: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                  // ClipRRect(
                  //   borderRadius: BorderRadius.circular(100),
                  //   child: Container(
                  //     height: 60,
                  //     width: 60,

                  //     child: Image.asset('assets/ma.png'),
                  //   ),
                  // ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          color: Colors.teal,
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/mapaa.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                              
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}

Widget cuadradoP(
    double w, double _sizeh, String data, String nombre) {
     
      

  // confirmeCase
  return Card(
    elevation: 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.centerLeft,
                    colors: [Colors.white, Colors.white])),
            height:(_sizeh > 640) ?  _sizeh / 6.5: _sizeh/6,
            // (_sizeh > 640) ? 140 : 130
            width: w / 3.5,
            // (_sizeh > 640) ? 150 : 140
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                      child: Text(
                    data,
                    style: TextStyle(
                      fontFamily: 'letra',
                      fontSize:(_sizeh > 640) ?  14:12,
                     
                      color: Colors.black54,
                      // letterSpacing: 2
                    ),
                  )),
                 
                  ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                          color: Colors.red.withOpacity(0.7),
                          height: (_sizeh > 640) ? 50:35,
                          width: (_sizeh > 640) ? 50:35,
                          image: AssetImage('assets/pendiente.png'))),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    
                      child: Text(nombre,
                          // data[0].confirmedCases.toString(),
                          style: TextStyle(
                              fontFamily: 'letra',
                              fontSize: 14,
                             
                              // letterSpacing: 2,
                              color: Colors.black54))),
                              
                ],
              ),
            ))),
  );
}
