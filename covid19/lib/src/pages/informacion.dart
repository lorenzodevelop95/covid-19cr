import 'package:covid19/src/Models/model.dart';
import 'package:covid19/src/providers/provider.dart';
import 'package:covid19/src/wiget/Homewidget/widget-cuadrados.dart';
import 'package:covid19/src/wiget/cuadrados.dart';
import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';

class Informacion extends StatefulWidget {
  Informacion({Key key}) : super(key: key);

  @override
  _InformacionState createState() => _InformacionState();
}

class _InformacionState extends State<Informacion> {
  final procesar = new Provider();
  double _sizew;
  double _sizeh;
  double height;

  @override
  Widget build(BuildContext context) {
    _sizew = MediaQuery.of(context).size.width;

    return demoPage();
  }

  Widget demoPage() {
    AppBar appBar = AppBar(
      backgroundColor: Colors.teal.shade600,
      centerTitle: true,
      title: Text(
        'Datos',
        style: TextStyle(
          fontFamily: 'letra',
          fontSize: 25,
          fontWeight: FontWeight.w800,
          color: Colors.white,
        ),
      ),
    );
    height = appBar.preferredSize.height;
    _sizeh = (MediaQuery.of(context).size.height) - (height);
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: appBar,
        body: FutureBuilder(
            future: procesar.getNoticias(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Datum>> snapshot) {
              if (snapshot.hasData) {
                final data = snapshot.data;
                return _home(data);
              } else {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.teal.shade600,
                        ),
                        width: 60,
                        height: 60,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text(
                          'Awaiting result...',
                          style: TextStyle(color: Colors.teal, fontSize: 20, fontFamily: 'letra'),
                        ),
                      )
                    ],
                  ),
                );
              }
            }));
  }

//
  Widget _home(List<Datum> data) {
    String dataH = data[0].byGender.men.toString();
    String dataM = data[0].byGender.women.toString();
    String data0 = data[0].byAge.adults.toString();
    String data1 = data[0].byAge.juveniles.toString();
    String data2 = data[0].byAge.elderlies.toString();
    String data3 = data[0].byNationality.costarricans.toString();
    String data4 = data[0].byNationality.foreigners.toString();
    String data5 = data[0].byNationality.pending.toString();

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height:2,
          ),
         condicionpa(_sizew, _sizeh, data3, 'Casos por genero'),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BounceInDown(child: cuadrados(_sizew, _sizeh, 'Mujeres', dataM)),
              SizedBox(
                width: _sizew / 6,
              ),
              BounceInDown(child: cuadradoH(_sizew, _sizeh, 'Hombres', dataH)),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          condicionpa(_sizew, _sizeh, data3, 'Casos por rango'),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BounceInLeft(child: widgetActivos(_sizew, _sizeh, data2, 'Persona Adulta Mayor','adulto')),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BounceInDown(child: cuadradoJ(_sizew, _sizeh, 'Adulta', data0)),
              SizedBox(
                width: _sizew / 7,
              ),
              BounceInDown(child: cuadradoN(_sizew, _sizeh, 'Joven', data1)),
            ],
          ),
         
          condicionpa(_sizew, _sizeh, data3, 'Casos por Nacionalidad'),
          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FadeInLeftBig(child: cuadradoc(_sizew, _sizeh, 'Costarricense', data3)),
              SizedBox(
                width: _sizew / 60,
              ),
              FadeInUpBig(child: cuadradoF(_sizew, _sizeh, 'Extranjero', data4)),
              SizedBox(
                width: _sizew / 60,
              ),
              FadeInRightBig(child: cuadradoP(_sizew, _sizeh, 'Pendiente', data5)),
            ],
          ),
        ],
      ),
    );
  }
}
