// import 'package:covid19/src/Models/carros_model.dart';
// import 'package:covid19/src/providers/provider.dart';
// import 'package:covid19/src/providers/providerdata.dart';
// import 'package:flutter/material.dart';

// class Restriccion extends StatefulWidget {
//   Restriccion({Key key}) : super(key: key);

//   @override
//   _RestriccionState createState() => _RestriccionState();
// }

// class _RestriccionState extends State<Restriccion> {
//   final carro = CarroProvedor();
//   double w;
//   double _sizeh;
//   double height;
//   int valoritem = 0;

//   @override
//   Widget build(BuildContext context) {
//     w = MediaQuery.of(context).size.width;
//     return scal(context, valoritem);
//   }

//   Widget scal(context, int index) {
//     AppBar appBar = AppBar(
//       backgroundColor: Colors.teal.shade600,
//       centerTitle: true,
//       title: Text(
//         'COSTA RICA C-19',
//         style: TextStyle(
//           fontFamily: 'letra',
//           fontSize: 25,
//           fontWeight: FontWeight.w800,
//           color: Colors.white,
//         ),
//       ),
//     );
//     height = appBar.preferredSize.height;
//     _sizeh = (MediaQuery.of(context).size.height) - (height);
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: appBar,
//       body: Column(
//         children: <Widget>[
//           //fondo(),
//           menu(),
//           home(context, index),
//         ],
//       ),
//     );
//   }

//   Widget fondo() {
//     return Row(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         Image(height: 150, image: AssetImage('assets/fondoc.png')),
//       ],
//     );
//   }

//   Widget menu() {
//     return DropdownButton<int>(
//         key: UniqueKey(),
//         items: [
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  0'),
//             value: 0,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  1'),
//             value: 1,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  2'),
//             value: 2,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  3'),
//             value: 3,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  4'),
//             value: 4,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  5'),
//             value: 5,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  6'),
//             value: 6,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  7'),
//             value: 7,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  8'),
//             value: 8,
//           ),
//           DropdownMenuItem<int>(
//             child: Text('Placa con terminación en:  9'),
//             value: 9,
//           ),
//         ],
//         onChanged: (int value) {
//           setState(() {
//             valoritem = value;
//           });
//         },
//         hint: Text('Selecciones el ultimo digito de la placa'),
//         value: valoritem
//         // _value,
//         );
//   }

//   Widget home(BuildContext context, int index) {
//     return FutureBuilder(
//         future: carro.getdata(),
//         builder: (context, AsyncSnapshot<List<Semaforo>> snapshot) {
//           final data = snapshot.data;
//           // no se puede cirular
//           return Card(
//             elevation: 5,
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
//             child: ClipRRect(
//                 borderRadius: BorderRadius.circular(20),
//                 child: Container(
//                     decoration: BoxDecoration(
//                         gradient: LinearGradient(
//                             begin: Alignment.bottomCenter,
//                             end: Alignment.centerLeft,
//                             colors: [Colors.white, Colors.white])),
//                     height: (_sizeh > 640) ? _sizeh / 3 : _sizeh / 2,
//                     // (_sizeh > 640) ? 140 : 130
//                     width: w / 2,
//                     // (_sizeh > 640) ? 150 : 140
//                     child: Container(
//                       padding: EdgeInsets.symmetric(vertical: 5),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: <Widget>[
//                           Center(
//                               child: Text(
//                             'No se puede circular',
//                             style: TextStyle(
//                               fontFamily: 'letra',
//                               fontSize: (_sizeh > 640) ? 14 : 12,

//                               color: Colors.black54,
//                               // letterSpacing: 2
//                             ),
//                           )),
//                           ClipRRect(
//                               child: Image(
//                                   height: (_sizeh > 640) ? 100 : 35,
//                                   width: (_sizeh > 640) ? 100 : 35,
//                                   image: AssetImage('assets/rojo.png'))),
//                           SizedBox(
//                             height: 5,
//                           ),
//                           Center(
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                             children: <Widget>[
//                               Text(data[valoritem].rojo.dia1.trim(),
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                               Text(data[valoritem].rojo.dia2,
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                               Text(data[valoritem].rojo.dia3,
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                               Text(data[valoritem].rojo.dia4,
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                               Text(data[valoritem].rojo.dia5,
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                               Text(data[valoritem].rojo.dia6,
//                                   style: TextStyle(
//                                       fontFamily: 'letra',
//                                       fontSize: 14,
//                                       color: Colors.black54)),
//                             ],
//                           )),
//                         ],
//                       ),
//                     ))),
//           );
//         });
//   }
// }
