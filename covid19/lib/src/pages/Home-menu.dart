import 'package:circle_bottom_navigation/circle_bottom_navigation.dart';
import 'package:circle_bottom_navigation/widgets/tab_data.dart';
import 'package:covid19/src/pages/HomePague.dart';
import 'package:covid19/src/pages/Restriccion.dart';
import 'package:covid19/src/pages/historial.dart';
import 'package:covid19/src/pages/info.dart';
import 'package:covid19/src/pages/informacion.dart';
import 'package:flutter/material.dart';


class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

 

  final _pages= [
      HomePague(),
      Informacion(),
       Historial(),
       Info()
  ];
 int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_currentIndex],
      bottomNavigationBar: CircleBottomNavigation(
        circleOutline: 0,
        circleSize: 45,
        arcHeight: 55,
        circleColor: Colors.teal,
        inactiveIconColor: Colors.grey,
        activeIconColor: Colors.white,
        barBackgroundColor: Colors.white,

        
        
  initialSelection: _currentIndex,
  tabs: [
    TabData(icon: Icons.home,iconSize: 35),
    TabData(icon: Icons.assessment,iconSize: 35),
    TabData(icon: Icons.history,iconSize: 35),
     TabData(icon: Icons.directions_car,iconSize: 35),
    TabData(icon: Icons.info, iconSize: 35),
  ],
  onTabChangedListener: (index) => setState(() => _currentIndex = index),
)
    );
  }
}


// BottomNavigationBar(
//         iconSize: 40,
//         elevation: 50,
// selectedFontSize: 10,

//         backgroundColor: Colors.teal,
//         currentIndex: _currentIndex,
//         items: [
//           BottomNavigationBarItem(
//             icon: Icon(
            
//             Icons.home,color: Colors.white.withOpacity(0.7),
            
//             ),
//           title: Text('Home',style: TextStyle(
//                 // fontFamily: 'letra',
//                 fontSize: 16,
//                 fontWeight: FontWeight.w800,
//                 color: Colors.white,
//                 // letterSpacing: 2
//                 ),),
//           backgroundColor: Colors.teal
          
//           ),
//            BottomNavigationBarItem(icon: Icon(Icons.assessment,color: Colors.white.withOpacity(0.7)),
//           title: Text('Data',style: TextStyle(
//                 // fontFamily: 'letra',
//                 fontSize: 16,
//                 fontWeight: FontWeight.w800,
//                 color: Colors.white,
//                 // letterSpacing: 2
//                 ),),
//           backgroundColor: Colors.teal
          
//           ),
//           BottomNavigationBarItem(icon: Icon(Icons.history,color: Colors.white.withOpacity(0.7),),

//           title: Text('Historial',style: TextStyle(
//                 // fontFamily: 'letra',
//                 fontSize: 16,
//                 fontWeight: FontWeight.w800,
//                 color: Colors.white,
//                 // letterSpacing: 2
//                 )),
//           backgroundColor: Colors.teal
//           )
//           ,
//            BottomNavigationBarItem(icon: Icon(Icons.info,color: Colors.white.withOpacity(0.7),),

//           title: Text('Info',style: TextStyle(
               
//                 fontSize: 16,
//                 fontWeight: FontWeight.w800,
//                 color: Colors.white,
//                 // letterSpacing: 2
//                 ),),
//           backgroundColor: Colors.teal
//           )
//         ],
//         onTap: (index){
//           setState(() {
//             _currentIndex= index;
//           });
          
//         },
//         ),