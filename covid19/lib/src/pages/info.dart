import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:animate_do/animate_do.dart';

class Info extends StatelessWidget {
  const Info({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.teal.shade600,
        centerTitle: true,
        title: Text(
          'About US',
          style: TextStyle(
              fontFamily: 'letra',
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
              // letterSpacing: 2
              ),
        ),
      ),
      body: _home(),
    );
  }

  Widget _home() {
    return SingleChildScrollView(
          child: BounceInDown(
                      child: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
               SizedBox(
                height: 20,
              ),
              Image(height: 200, image: AssetImage('assets/dos.png')),
              Text(
                'COSTA RICA C-19',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 30,
                    fontWeight: FontWeight.w800,
                    color: Colors.teal.shade600,
                    // letterSpacing: 2
                    ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.only(left: 10),
                                child: Text(
                    'Costa Rica C-19, es un aplicación creada con la intención de informar sobre los casos de C-19 vigentes en el país y que toda la población se mantenga informada del desarrollo de este problema que enfrenta Costa Rica.',
                    style: TextStyle(
                        fontFamily: 'letra',
                        fontSize: 14,
                        
                        color: Colors.teal.shade600,
                        // letterSpacing: 2
                        ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Colaboradores',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 25,
                    fontWeight: FontWeight.w800,
                    color: Colors.teal.shade600,
                    // letterSpacing: 2
                    ),
              ),
               SizedBox(
                height: 20,
              ),
              Text(
                'Devep móvil: Lorenzo Carazo Zuñiga ',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 18,
                    
                    color: Colors.teal.shade600,
                    // letterSpacing: 1
                    ),
              ),
              SizedBox(
                height: 10,
              ),
              Text("Contacto: lorenzodevelop@gmail.com",
                  style: TextStyle(
                      fontFamily: 'letra',
                      fontSize: 12,
                      
                      color: Colors.teal.shade600,
                      // letterSpacing: 2
                      )),
                       SizedBox(
                height: 20,
              ),
              Text(
                'Base de datos: Kevin wolf ',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 20,
                    
                    color: Colors.teal.shade600,
                    // letterSpacing: 0
                    ),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                child: Text(
                'Toca y visita su página! : https://kevinwolf.dev/',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 12,
                    
                    color: Colors.teal.shade600,
                    // letterSpacing: 1
                    ),
              ),
                onTap: () async {
                  if (await canLaunch("https://kevinwolf.dev/?fbclid=IwAR3_mQNm88A65qGowuS_3TGXrB0ngMJazVW3NKhdnSHLu4oMWZVRFw7YRuc")) {
                    await launch("https://kevinwolf.dev/?fbclid=IwAR3_mQNm88A65qGowuS_3TGXrB0ngMJazVW3NKhdnSHLu4oMWZVRFw7YRuc");
                  }
                },
              ),
               SizedBox(
                height: 20,
              ),
              Text(
                'Imagenes: Justin Miles Mendoza ',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 20,
                  
                    color: Colors.teal.shade600,
                    // letterSpacing: 0
                    ),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                child: Text(
                'Toca y visita su Instagram! : @Mendoza.pics',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 12,
                   
                    color: Colors.teal.shade600,
                    // letterSpacing: 1
                    ),
              ),
                onTap: () async {
                  if (await canLaunch("https://www.instagram.com/invites/contact/?i=1kv5ve8vkzk0u&utm_content=2n3svx4")) {
                    await launch("https://www.instagram.com/invites/contact/?i=1kv5ve8vkzk0u&utm_content=2n3svx4");
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
               Text(
                '© 2020 Lorenzo Carazo Zuñiga ',
                style: TextStyle(
                    fontFamily: 'letra',
                    fontSize: 10,
                  
                    color: Colors.teal.shade600,
                    // letterSpacing: 0
                    ),
              ),
            ],
        ),
      ),
          ),
    );
  }
}
