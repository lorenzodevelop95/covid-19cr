import 'package:animate_do/animate_do.dart';
import 'package:covid19/src/providers/providerdata.dart';
import 'package:flutter/material.dart';

import 'package:covid19/src/Models/model.dart';
import 'package:covid19/src/providers/provider.dart';
import 'package:covid19/src/wiget/Homewidget/widget-Home.dart';
import 'package:covid19/src/wiget/Homewidget/widget-cuadrados.dart';

class HomePague extends StatefulWidget {
  const HomePague({Key key}) : super(key: key);

  @override
  _HomePagueState createState() => _HomePagueState();
}

class _HomePagueState extends State<HomePague> {
  double _sizew;
  double _sizeh;
  double height;
  final casos = Provider();
  final carros = CarroProvedor();
  @override
  Widget build(BuildContext context) {
    _sizew = MediaQuery.of(context).size.width;
   
    return demoPage();
  }

  Widget demoPage() {
    AppBar appBar = AppBar(
      backgroundColor: Colors.teal.shade600,
      centerTitle: true,
      title: Text(
        'COSTA RICA C-19',
        style: TextStyle(
          fontFamily: 'letra',
          fontSize: 25,
          fontWeight: FontWeight.w800,
          color: Colors.white,
        ),
      ),
    );
    height = appBar.preferredSize.height;
    _sizeh = (MediaQuery.of(context).size.height) - (height);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      body: _home(context),
    );
  }

  Widget _home(BuildContext context) {
   
    return FutureBuilder(
        future: casos.getNoticias(),
        builder: (context, AsyncSnapshot<List<Datum>> snapshot) {
          final data = snapshot.data;
          if (snapshot.hasData) {
            String data0 = data[0].confirmedCases.toString();
            String data1 = data[0].discardedCases.toString();
            String data2 = data[0].byStatus.active.toString();
            String data3 = data[0].byStatus.recovered.toString();
            String data4 = data[0].byStatus.deceased.toString();

            return Center(
              child: Container(
                child: Stack(
                  children: <Widget>[
                    //right: (_sizeh > 640) ? 0.1 : 50,
                    Positioned(
                        // top: 0,
                        child: fondoPrincipal(_sizew, _sizeh, data)),
                    // Positioned(left: 10, bottom: 100, child: confirmeCase(data)),
                    Positioned(
                        bottom: (_sizeh > 640) ? _sizeh / 2.9 : _sizeh / 3.2,
                        left: _sizew / 7,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            FadeInLeft(
                             
                              child: widgetcuadradoprincipal(_sizew, _sizeh,
                                  data0, 'Confirmados', 'activo'),
                            ),
                            SizedBox(
                              width: _sizew / 7.6,
                            ),
                            FadeInRight(
                             
                              child: widgetcuadradoprincipal(_sizew, _sizeh,
                                  data1, 'Descartados', 'descartado'),
                            ),
                          ],
                        )),

                    Positioned(
                        bottom: (_sizeh > 640) ? _sizeh / 7 : _sizeh / 7,
                        left: _sizew / 4,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            FadeInDown(
                             
                              child: widgetActivos(_sizew, _sizeh, data2,
                                  'Pacientes Activos', 'activos'),
                            ),
                          ],
                        )),

                    Positioned(
                        bottom: (_sizeh > 640) ? _sizeh / 25 : _sizeh / 40,
                        right: _sizew * 0.02,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // prueba(_sizeh, data2, 'Activos'),
                            FadeInUpBig(
                              
                              child: recuperados(_sizew, _sizeh, data3,
                                  'Pacientes Recuperados'),
                            ),
                            SizedBox(
                              width: _sizew / 9,
                            ),
                            FadeInUpBig(
                             
                              child: recuperados(_sizew, _sizeh, data4,
                                  'Pacientes Fallecidos'),
                            ),
                          ],
                        )),

                    Positioned(
                        bottom: (_sizeh > 640) ? _sizeh / 3.7 : _sizeh / 4,
                        left: _sizew / 19,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // prueba(_sizeh, data2, 'Activos'),
                            condicionpa(_sizew, _sizeh, data3,
                                'Condición de pacientes'),
                          ],
                        )),
                    Positioned(
                        bottom: _sizeh / 1.9,
                        left: _sizew / 50,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // prueba(_sizeh, data2, 'Activos'),
                            condicioncaso(
                                _sizew, _sizeh, data3, 'Estado de Casos'),
                          ],
                        ))
                  ],
                ),
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.teal.shade600,
                    ),
                    width: 60,
                    height: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text(
                      'Awaiting result...',
                      style: TextStyle(
                          color: Colors.teal,
                          fontSize: 20,
                          fontFamily: 'letra'),
                    ),
                  )
                ],
              ),
            );
          }
        });
  }
}
