import 'package:covid19/src/Models/model.dart';
import 'package:covid19/src/providers/provider.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class Historial extends StatefulWidget {
  Historial({Key key}) : super(key: key);

  @override
  _HistorialState createState() => _HistorialState();
}

class _HistorialState extends State<Historial> {
  final procesar = new Provider();
  double _sizew;
  double _sizeh;
  @override
  Widget build(BuildContext context) {
    _sizeh = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.teal.shade600,
        centerTitle: true,
        title: Text('Historial',
            style: TextStyle(
              fontFamily: 'letra',
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
              // letterSpacing: 2
            )),
      ),
      body: FutureBuilder(
          future: procesar.getNoticias(),
          builder: (BuildContext context, AsyncSnapshot<List<Datum>> snapshot) {
            if (snapshot.hasData) {
              final data = snapshot.data;
              return ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        BounceInLeft(
                                                  child: Card(
                            elevation: 10,
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.bottomLeft,
                                      end: Alignment.bottomRight,
                                      colors: [Colors.white, Colors.white])),
                              width: 400,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Colors.teal.shade600,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Fecha publicación :',
                                          style: TextStyle(fontWeight: FontWeight.w800,fontFamily: 'letra',
                                              color: Colors.white, fontSize: 20),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Text(
                                            '${data[index].date.day.toString()}/',
                                            style: TextStyle(
                                                color: Colors.white,fontFamily: 'letra',
                                                fontSize: 20,fontWeight: FontWeight.w800,)),
                                        Text(
                                            '${data[index].date.month.toString()}/',
                                            style: TextStyle(fontFamily: 'letra',
                                                color: Colors.white,fontWeight: FontWeight.w800,
                                                fontSize: 20)),
                                        Text(
                                            '${data[index].date.year.toString()}',
                                            style: TextStyle(fontFamily: 'letra',
                                                color: Colors.white,fontWeight: FontWeight.w800,
                                                fontSize: 20))
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 200,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Actividad :',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontFamily: 'letra',
                                                 
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Casos Confirmados =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text('  ${data[index].confirmedCases}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Casos Descartados =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text('  ${data[index].discardedCases}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 200,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Estado de los casos :',
                                              style: TextStyle(fontFamily: 'letra',
                                                  
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Activos =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byStatus.active}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Recuperados =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byStatus.recovered}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Muertes =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byStatus.deceased}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 200,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Casos por genero :',
                                              style: TextStyle(fontFamily: 'letra',
                                                  
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Mujeres =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byGender.women}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Hombres =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byGender.men}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 200,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Casos por rango :',
                                              style: TextStyle(fontFamily: 'letra',
                                                 
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Adultos Mayores  =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byAge.elderlies}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Adultos =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byAge.adults}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Jovenes =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byAge.juveniles}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 210,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Casos por Nacionalidad :',
                                              style: TextStyle(fontFamily: 'letra',
                                                  
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Costarricense =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(
                                          ' ${data[index].byNationality.costarricans}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Extranjero =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(
                                          ' ${data[index].byNationality.foreigners}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Pendiente =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(
                                          ' ${data[index].byNationality.pending}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 200,
                                      color: Colors.teal.shade600,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(' Casos por Provincia :',
                                              style: TextStyle(fontFamily: 'letra',
                                                 
                                                  color: Colors.white,
                                                  fontSize: _tama(_sizeh))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Punatarenas =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(
                                          ' ${data[index].byLocation.puntarenas}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' San José =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.sanJose}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Heredia =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.heredia}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Guanacaste =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(
                                          ' ${data[index].byLocation.guanacaste}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Limón =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.limon}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Alajuela =',
                                          style: TextStyle(fontFamily: 'letra',
                                             
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.alajuela}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Cartago =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.cartago}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(' Desconocido =',
                                          style: TextStyle(fontFamily: 'letra',
                                              
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                      Text(' ${data[index].byLocation.unknown}',
                                          style: TextStyle(fontFamily: 'letra',
                                              color: Colors.teal.shade600,
                                              fontSize: _tama(_sizeh))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            } else {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.teal.shade600,
                      ),
                      width: 60,
                      height: 60,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                          'Awaiting result...',
                          style: TextStyle(color: Colors.teal, fontSize: 20, fontFamily: 'letra'),
                        ),
                    )
                  ],
                ),
              );
            }
          }),
    );
  }

  double _tama(double size) {
    double tama;
    if (size > 640 && size > 740) {
      tama = 16;
    } else if (size > 640 && size < 740) {
      tama = 16;
    } else {
      tama = 12;
    }

    return tama;
  }

 
}
