import 'dart:convert';
import 'package:covid19/src/Models/model.dart';
import 'package:http/http.dart' as http;


class Provider {
  Future<List<Datum>> getNoticias() async {
    String _url =
        'https://coronaviruscr.com/api/reports?fbclid=IwAR0uA7pNCt_0tIQy4OH-9FdBmovHo8wKm4vST9FeTktU5JcOp7T6BF_RdIg';

    final response = await http.get(_url);
    if(response.statusCode == 200){
    final decodedata = json.decode(response.body);
    final casos = new CoronaModel.fromJsonList(decodedata['data']);
    //print(casos.casos[0].date.toString());
    
    return casos.casos;
    }else{
      throw Exception('Error al conectar con el servidor');
    }

  }
// <List<Id>>



}


