import 'package:covid19/src/Models/carros_model.dart';
import 'package:covid19/src/providers/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CarroProvedor {
  Future<List<Semaforo>> getdata() async {
    String _url = 'https://ucrbeta-25da1.firebaseio.com/carro.json';

    final response = await http.get(_url);
    if (response.statusCode == 200) {
      final decodedata = json.decode(response.body);
      final data = Semaforos.fromjson(decodedata);
      
      return data.data;
    } else {
      throw Exception('Error al conectar con el servidor');
    }
  }
}
