
class Semaforos{

 List<Semaforo> data;

 Semaforos({
   this.data
 });

 factory Semaforos.fromjson(Map<String,dynamic> json){
   var list = json["placas"] as List;
    List<Semaforo> listaroja = list.map((i) => Semaforo.fromjson(i)).toList();
   return Semaforos(
     data: listaroja
   );
 }


}


class Semaforo {
  int placas;
  SemaforoRojo rojo;
  SemaforoAmarillo amarillo;
  SemaforoVerde verde;

  Semaforo({this.placas, this.rojo, this.amarillo, this.verde});

  factory Semaforo.fromjson(Map<String, dynamic> fromjson) {
    return Semaforo(
        
        rojo: SemaforoRojo.fromjson(fromjson["Rojo"]),
        amarillo: SemaforoAmarillo.fromjson(fromjson["Amarillo"]),
        verde: SemaforoVerde.fromjson(fromjson["Verde"]),
        placas: fromjson["placa"],
        
        );
  }
}

class SemaforoRojo {
  String dia1;
  String dia2;
  String dia3;
  String dia4;
  String dia5;
  String dia6;

  SemaforoRojo({
    this.dia1,
    this.dia2,
    this.dia3,
    this.dia4,
    this.dia5,
    this.dia6,
  });

  factory SemaforoRojo.fromjson(Map<String, dynamic> fromjson) {
    return SemaforoRojo(
      dia1: fromjson["dia1"],
      dia2: fromjson["dia2"],
      dia3: fromjson["dia3"],
      dia4: fromjson["dia4"],
      dia5: fromjson["dia5"],
      dia6: fromjson["dia6"],
    );
  }
}

class SemaforoAmarillo {
  String dia1;

  SemaforoAmarillo({this.dia1});

  factory SemaforoAmarillo.fromjson(Map<String, dynamic> fromjson) {
    return SemaforoAmarillo(dia1: fromjson["dia1"]);
  }
}

class SemaforoVerde {
  String dia1;
  String dia2;

  SemaforoVerde({this.dia1, this.dia2});

  factory SemaforoVerde.fromjson(Map<String, dynamic> fromjson) {
    return SemaforoVerde(dia1: fromjson["dia1"], dia2: fromjson["dia2"]);
  }
}
