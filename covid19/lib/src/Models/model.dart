// To parse this JSON data, do
//
//     final covidmodel = covidmodelFromJson(jsonString);

import 'dart:convert';


class CoronaModel {

 List<Datum> casos = List();

 CoronaModel();

 CoronaModel.fromJsonList(List<dynamic> json){

  if(json == null) return;

  for(final item in json){
    final pelicula = new Datum.fromJson(item);
    casos.add(pelicula);
  }


 }

}

class Datum {
    DateTime date;
    int confirmedCases;
    int discardedCases;
    ByStatus byStatus;
    ByGender byGender;
    ByAge byAge;
    ByNationality byNationality;
    ByLocation byLocation;

    Datum({
        this.date,
        this.confirmedCases,
        this.discardedCases,
        this.byStatus,
        this.byGender,
        this.byAge,
        this.byNationality,
        this.byLocation,
        
    });

     Datum.fromJson(Map<String, dynamic> json){
        date            = DateTime.parse(json["date"]);
        confirmedCases  =json["confirmedCases"];
        discardedCases  = json["discardedCases"];
        byStatus        = ByStatus.fromJson(json["byStatus"]);
        byGender        = ByGender.fromJson(json["byGender"]);
        byAge           = ByAge.fromJson(json["byAge"]);
        byNationality   = ByNationality.fromJson(json["byNationality"]);
        byLocation      = ByLocation.fromJson(json["byLocation"]);
     }


    // Map<String, dynamic> toJson() => {
    //     "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    //     "confirmedCases": confirmedCases,
    //     "discardedCases": discardedCases,
    //     "byStatus": byStatus.toJson(),
    //     "byGender": byGender.toJson(),
    //     "byAge": byAge.toJson(),
    //     "byNationality": byNationality.toJson(),
    //     "byLocation": byLocation.toJson(),
    // };
}

class ByAge {
    int juveniles;
    int adults;
    int elderlies;

    ByAge({
        this.juveniles,
        this.adults,
        this.elderlies,
    });

    factory ByAge.fromJson(Map<String, dynamic> json) => ByAge(
        juveniles: json["juveniles"],
        adults: json["adults"],
        elderlies: json["elderlies"],
    );

    Map<String, dynamic> toJson() => {
        "juveniles": juveniles,
        "adults": adults,
        "elderlies": elderlies,
    };
}

class ByGender {
    int women;
    int men;

    ByGender({
        this.women,
        this.men,
    });

    factory ByGender.fromJson(Map<String, dynamic> json) => ByGender(
        women: json["women"],
        men: json["men"],
    );

    Map<String, dynamic> toJson() => {
        "women": women,
        "men": men,
    };
}

class ByLocation {
    int sanJose;
    int alajuela;
    int cartago;
    int heredia;
    int guanacaste;
    int puntarenas;
    int limon;
    int unknown;

    ByLocation({
        this.sanJose,
        this.alajuela,
        this.cartago,
        this.heredia,
        this.guanacaste,
        this.puntarenas,
        this.limon,
        this.unknown,
    });

    factory ByLocation.fromJson(Map<String, dynamic> json) => ByLocation(
        sanJose: json["sanJose"],
        alajuela: json["alajuela"],
        cartago: json["cartago"],
        heredia: json["heredia"],
        guanacaste: json["guanacaste"],
        puntarenas: json["puntarenas"],
        limon: json["limon"],
        unknown: json["unknown"] == null ? null : json["unknown"],
    );

    Map<String, dynamic> toJson() => {
        "sanJose": sanJose,
        "alajuela": alajuela,
        "cartago": cartago,
        "heredia": heredia,
        "guanacaste": guanacaste,
        "puntarenas": puntarenas,
        "limon": limon,
        "unknown": unknown == null ? null : unknown,
    };
}

class ByNationality {
    int costarricans;
    int foreigners;
    int pending;

    ByNationality({
        this.costarricans,
        this.foreigners,
        this.pending
    });

    factory ByNationality.fromJson(Map<String, dynamic> json) => ByNationality(
        costarricans: json["costarricans"],
        foreigners: json["foreigners"],
        pending: json["pending"],
    );

    Map<String, dynamic> toJson() => {
        "costarricans": costarricans,
        "foreigners": foreigners,
        "pending": pending,
    };
}

class ByStatus {
    int active;
    int recovered;
    int deceased;

    ByStatus({
        this.active,
        this.recovered,
        this.deceased,
    });

    factory ByStatus.fromJson(Map<String, dynamic> json) => ByStatus(
        active: json["active"],
        recovered: json["recovered"],
        deceased: json["deceased"],
    );

    Map<String, dynamic> toJson() => {
        "active": active,
        "recovered": recovered,
        "deceased": deceased,
    };
}


